
<!-- README.md is generated from README.Rmd. Please edit that file -->

# butinagepei <img src="man/figures/logo.png" align="right" height="99"/>

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)

<!-- badges: end -->

The goal of butinagepei is to explore bee harvesting in Reunion island.

These data come from a participative [facebook
group](https://www.facebook.com/groups/375179256507926/?multi_permalinks=571229623569554%2C570660930293090%2C570660510293132%2C570658770293306&notif_id=1594800619596841&notif_t=group_activity&ref=notif)
where members (mostly beekeepers) add their observations of bees giving
the following information:

-   the plant species which they are pollinating, along with a picture
    to check its identification
-   the place and time at which it did occur

## Usage

### Online

Just go to [this website](https://doana-r.shinyapps.io/butinagepei/)!

### Locally

If you prefer to use the app locally, you can install the last released
version of butinagepei from
[gitlab](https://gitlab.com/doana-r/butinagepei) with this R code:

``` r
remotes::install_gitlab("doana-r/butinagepei")
```

then run:

``` r
butinagepei::run_app()
```
