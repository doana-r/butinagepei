% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data.R
\docType{data}
\name{suivi}
\alias{suivi}
\title{Spatio-temporal tracking of pollinated plants}
\format{
A data frame with 224 rows and 10 variables:
\describe{
\item{date}{date of the observation}
\item{latitude,longitude}{coordinates of the observation}
\item{g_latin, e_latin}{latin name of the species}
\item{espece}{unique code of the species name : 6 capital letters. 3 for the family and 3 for the species}
\item{vernaculaire}{common name of the species}
\item{statut}{species status : native or exotic}
\item{img_src}{path to an image of the species}
\item{altitude}{observation elevation}
}
}
\source{
\url{https://www.facebook.com/groups/375179256507926/?multi_permalinks=571229623569554\%2C570660930293090\%2C570660510293132\%2C570658770293306&notif_id=1594800619596841&notif_t=group_activity&ref=notif}
}
\usage{
suivi
}
\description{
A dataset containing the pollinated plant species recorded by participatory science
}
\keyword{datasets}
