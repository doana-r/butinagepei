## Présentation

**Butinage Péi** est une application permettant d'explorer les plantes butinées par les abeilles sur l'île de la Réunion.

Ces données participatives proviennent du groupe facebook [**Observatoire des interactions plantes/abeilles à La Réunion**](https://www.facebook.com/groups/375179256507926/?multi_permalinks=571229623569554%2C570660930293090%2C570660510293132%2C570658770293306&notif_id=1594800619596841&notif_t=group_activity&ref=notif) où ses membres peuvent apporter leurs observations d'abeilles avec les informations suivantes :

- l'espèce de plante qu'elles butinent avec une photo pour vérifier l'identification de la plante
- le lieu et la date de l'événement


## Mode d'emploi

### Onglet Carte interactive

#### La carte

Chaque point de la carte interactive représente une observation d'abeille(s). 
En cliquant dessus, vous aurez accès à des informations plus précises : le nom commun et latin de la plante butinée, la date de l'observation et une image représentative de la plante.

Lorsque les points sont trop proches les uns des autres, ils se regroupent dans un *cluster* dont la couleur et le nombre affiché indiquent combien d'observations sont regroupées.
Pour aller voir de plus près les observations d'un *cluster*, il suffit de cliquer dessus ou de zoomer en direction de ce *cluster*.


#### Le panneau de droite

Vous pouvez choisir :
- la période de temps pendant laquelle vous voulez explorer le butinage des abeilles
- l'espèce de plante qui vous intéresse

Les informations contenues dans la carte et le graphique sont mises à jour en temps réel.

Le graphique "Evolution temporelle des observations" est une courbe de densité des observations de l'espèce sélectionnée sur la plage de temps choisie. 
Chaque trait vertical en bas de la figure représente une observation.


### Onglet Données


Le tableau recensant chaque observation comprend les informations suivantes :

- La date de l'observation
- Ses coordonnées approximatives (latitude et longitude avec le système de coordonnées WGS84)
- Le nom commun et le nom latin de l'espèce butinée
- Le statut de l'espèce selon le [Conservatoire Botanique National Mascarin](https://www.cbnm.org/) : indigène ou exotique
- La source de l'image représentative de l'espèce qui est affichée sur la carte


Vous pouvez filtrer les lignes du tableau qui vous intéressent en cliquant sur les champs présents sous les noms des colonnes.

Les données sont disponibles sous licence [Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/), © [GDS Réunion](https://www.gds974.com/).  
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>

Cela signifie que vous pouvez partager et adapter ces données sous condition de les créditer et de les diffuser sous la même licence.


## En savoir plus

Cette application est financée par le [GDS Réunion](https://www.gds974.com/).

Le développement de cette application en est encore au stade expérimental.
Son utilisation et les données employées sont susceptibles de varier.

<!-- badges: start -->
[![Développement: expérimental](https://img.shields.io/badge/Développement-expérimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

Le [code source](https://gitlab.com/doana-r/butinagepei) de l'application est sous licence MIT, © [DoAna - Statistiques Réunion](https://doana-r.com/).  



N'hésitez pas à [signaler un bug ou suggérer des améliorations](https://gitlab.com/doana-r/butinagepei/issues).


Crédit logo : [DoAna - Statistiques Réunion](https://doana-r.com/)



<!-- 

Les données sont anonymisées

-->


