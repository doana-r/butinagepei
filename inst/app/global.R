suppressPackageStartupMessages({
  library(shiny)
  library(htmltools)
  library(DT)
  library(dplyr)
  library(ggplot2)
  library(leaflet)
  library(butinagepei)
})


# deployment to shinyapps.io with rsconnect

# SETUP
# git remote set-url --add --push origin git@gitlab.com:doana-r/butinagepei.git
# git remote set-url --add --push origin https://github.com/anna-doizy/butinagepei.git # en git@ ce serait mieux
# git remote -v
#> origin  git@gitlab.com:doana-r/butinagepei.git (fetch)
#> origin  https://github.com/anna-doizy/butinagepei.git (push)
#> origin  git@gitlab.com:doana-r/butinagepei.git (push)

# EACH TIME before deployment
# update all packages
# check and install butinagepei package
# check if parse("inst/app/server.R"), parse("inst/app/ui.R"), parse("inst/app/global.R") work
# commit & push
# restart R session
# remotes::install_github("anna-doizy/butinagepei")
# publish the app

# nécessaire car comme l'application charge le package pour démarrer (récupère le jeu suivi), il a besoin d'être installé proprement dans le serveur distant. Pour l'instant RStudio (shinyapps) ne permet de faire cela qu'avec des packages classiques (CRAN) ou github, mais pas gitlab...



